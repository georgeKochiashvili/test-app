import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoviesDataComponent } from './components/movies-data/movies-data.component';
import { DevicesListComponent} from './components/devices-list/devices-list.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';

const routes: Routes = [
  {
    path: '',
    component: DevicesListComponent
  },
  {
    path: 'movies',
    component: MoviesDataComponent
  },
  {
    path: 'devices',
    component: DevicesListComponent
  },
  {
    path: 'todo',
    component: TodoListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { initialNavigation: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
