import { TestBed } from '@angular/core/testing';

import { JsonReaderService } from './json-reader.service';

describe('JsonReader.ServiceService', () => {
  let service: JsonReaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JsonReaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
