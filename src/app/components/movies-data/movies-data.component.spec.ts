import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoviesDataComponent } from './movies-data.component';

describe('MoviesDataComponent', () => {
  let component: MoviesDataComponent;
  let fixture: ComponentFixture<MoviesDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoviesDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoviesDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
