import { Component, OnInit } from '@angular/core';

import { JsonReaderService } from '../../services/json-reader.service';
import { Subscription } from 'rxjs';
import { NestedTreeControl } from '@angular/cdk/tree';
import { ArrayDataSource } from '@angular/cdk/collections';

@Component({
  selector: 'app-movies-data',
  templateUrl: './movies-data.component.html',
  styleUrls: ['./movies-data.component.less']
})
export class MoviesDataComponent implements OnInit {
  treeControl = new NestedTreeControl ((node: any) => node.children);
  subscription: Subscription;
  treeData;

  constructor(private jsonReaderService: JsonReaderService) { }

  ngOnInit(): void {
    this.subscription = this.jsonReaderService.readJSON('./assets/movies-data.json').subscribe((result) => {
      this.treeData = this.formTree(result);
      this.treeData = new ArrayDataSource(this.treeData);
    });
  }

  formTree(data): any {
    // go through all categories
    return data.categories.map((category) => {
      category.children = [];
      // find movies which are in current category and add them as children
      data.movies.find((movie) => {
        if (movie.categories.includes(category.id)) {
          category.children.push({
            name: movie.name
          });
        }
      });
      // sort
      category.children.sort((a, b) => a.name.localeCompare(b.name));
      return category;
    });
  }

  hasChild = (_: number, node: any) => !!node.children && node.children.length > 0;

}
