import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { JsonReaderService } from '../../services/json-reader.service';
import { Subscription } from 'rxjs';
import { MatSort } from '@angular/material/sort';

import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-devices-list',
  templateUrl: './devices-list.component.html',
  styleUrls: ['./devices-list.component.less']
})
export class DevicesListComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort, {static: false}) set content(sort: MatSort) {
    if (this.deviceData) {
      this.deviceData.sort = sort;
    }
  }
  subscription: Subscription;
  displayedColumns: string[];
  display = 'grid';
  selectedSorting;
  deviceData: any = [];
  deviceGridData: any;
  constructor(private jsonReaderService: JsonReaderService) { }

  ngOnInit(): void {
    this.displayedColumns = ['id', 'name', 'type', 'price'];
    this.subscription = this.jsonReaderService.readJSON('./assets/devices.json').subscribe((result) => {
      this.deviceGridData = result.devices;
      this.deviceData = new MatTableDataSource(result.devices);

      /* For proper alphabetical sorting we convert all data to lowercase
         For proper price sorting we cut currency symbol from price
       */
      this.deviceData.sortingDataAccessor = (data, attribute) => {
        const formattedData = data[attribute].toLowerCase();
        return attribute === 'price' ? this.parsePrice(formattedData) : formattedData;
      };

      this.deviceData.filterPredicate = (data: any, filterValue: string) => {
        return data.name
          .trim()
          .toLocaleLowerCase()
          .includes(filterValue.trim()
          .toLocaleLowerCase());
      };
    });
  }

  filter(value: string): void {
    this.deviceData.filter = value;
  }

  ngOnDestroy(): void {
    this.subscription?.unsubscribe();
  }

  sortData(): void {
    const columnName = this.selectedSorting.columnName;
    const direction = this.selectedSorting.order;
    this.deviceData.filteredData.sort((a, b) => {
      let compareResult;
      let valueA = a[columnName].toLowerCase();
      let valueB = b[columnName].toLowerCase();
      if (columnName === 'price') {
       valueA = this.parsePrice(valueA);
       valueB = this.parsePrice(valueB);
      }
      if (valueA > valueB) { compareResult =  1; }
      if (valueA < valueB) { compareResult =  -1; }

      return compareResult * (direction === 'asc' ? 1 : -1);
    });
  }

  parsePrice(price): number {
    return isNaN(price) ? parseInt(price.match(/(\d)*$/)[0], 10) : price;
  }

  switchView(): void {
    // clear filter for table before switching
    if (this.deviceData) {
      this.deviceData.filter = undefined;
    }
    this.display = this.display === 'grid' ? 'table' : 'grid';
  }
}

