import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.less']
})
export class TodoListComponent implements OnInit {
  list = [];
  todoForm;
  error;
  formControls: {[key: string]: AbstractControl};

  constructor() { }

  ngOnInit(): void {
    this.todoForm = new FormGroup({
      taskName: new FormControl('', [Validators.required])
    });
    this.formControls = this.todoForm.controls;
    this.loadTaskList();
  }

  addTask(input, status = 'To Do'): void {
    if (!this.isDuplicate(input.value)) {
      this.list.push({ name: input.value, status });
      this.formControls.taskName.setValue('');
      this.clearFormStateAndErrors();
      this.save();
    } else {
      this.error = true;
    }
  }

  deleteTask(index): void {
    this.list.splice(index, 1);
    this.save();
  }

  changeTaskStatus(index, newStatus): void {
    this.list[index].status = newStatus;
    this.save();
  }

  isDuplicate(name): boolean {
    return this.list.some((task) => {
      return task.name === name;
    });
  }

  clearFormStateAndErrors(): void {
    this.error = false;
    this.todoForm.markAsUntouched();
    this.todoForm.markAsPristine();
  }

  loadTaskList(): void {
    this.list = JSON.parse(localStorage.getItem('todoList')) || [];
  }

  save(): void {
    localStorage.setItem('todoList', JSON.stringify(this.list));
  }

  hasError = (controlName: string, errorName: string) => {
    return this.todoForm.controls[controlName].hasError(errorName);
  }
}
